# max.datatype.com

This repository contains some data types commonly used when communicating : address, connection,
message ... This module is completely independent, which makes it reusable in other contexts than
MAX.

Functional layers like [P2P](https://cea-licia.gitlab.io/max/max.gitlab.io/library/network/peer-to-peer/) and [Generic Blockchain](https://cea-licia.gitlab.io/max/max.gitlab.io/library/blockchain/generic/) use this module.

## About MAX

__MAX__ (for 'Multi-Agent eXperimenter') is a framework for agent-based simulation and full
agent-based applications building. The objective of __MAX__  is to make rapid prototyping of
industrial cases and to carry out their feasibility analysis in a realistic manner.

__MAX__ is particularly well suited for modeling open, distributed and intelligent systems
developing over time. It embeds the Agent/Environment/Role (AER) organizational model, includes
tools and components useful for developing and testing simulation models and is written in Java 11
to run on any device.

You want to know if MAX is suited for your project ? Check our [website](https://cea-licia.gitlab.io/max/max.gitlab.io) to find it out!

## Getting Started

To include this model in your application, here is what you need to add in your pom file:

```xml
<!-- Add the MAX group package repository. -->
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/16222017/-/packages/maven</url>
  </repository>
</repositories>
  
...

<dependencies>
    <dependency>
      <groupId>max.datatype</groupId>
      <artifactId>com</artifactId>
      <version>${max.datatype.com.version}</version>
    </dependency>
</dependencies>
```

# Licence

__max.datatype.com__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)
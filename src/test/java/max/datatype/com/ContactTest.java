/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author Önder Gürcan
 *
 */
public class ContactTest {

	@Test
	public void createNamedValid() {
		Contact contact = new Contact("Alice");

		assertNotNull(contact);
		assertEquals("Alice", contact.getName());
		assertNull(contact.getAddress(Contact.DEFAULT));
	}

	@Test
	public void createInvalid() {
		try {
			new Contact(null);
			fail("This line should not be processed.");
		} catch (IllegalArgumentException e) {
			assertEquals(Contact.E_NULL_CONTACT_NAME, e.getMessage());
		}
	}

	@Test
	public void createUnnamedValid() {
		Contact contact = new Contact();

		assertNotNull(contact);
		assertEquals(Contact.NONAME, contact.getName());
		assertNull(contact.getAddress(Contact.DEFAULT));
	}

	@Test
	public void addAddressValid() {
		Contact contact = new Contact("Alice");

		Address address1 = new UUIDAddress();
		contact.addAddress(Contact.DEFAULT, address1);

		Address address2 = contact.getAddress(Contact.DEFAULT);
		assertNotNull(address2);
		assertEquals(address2, address1);
	}

	@Test
	public void addAddressInvalid() {
		try {
			Contact contact = new Contact("Alice");
			Address address1 = new UUIDAddress();
			contact.addAddress(null, address1);
			fail("This line should not be processed.");
		} catch (IllegalArgumentException e) {
			assertEquals(Contact.E_NULL_ADDRESS_TYPE, e.getMessage());
		}
	}

}

/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

/**
 * An address is an identifier of unique integer values that represents a
 * possible destination for a payment.
 * 
 * @author Önder Gürcan
 *
 */
public class AddressTest {

	@Test
	public void generateValid() {
		Address address1 = new UUIDAddress();
		assertNotNull(address1);

		Address address2 = new UUIDAddress();
		assertNotNull(address2);

		assertNotEquals(address1, address2);
	}

}

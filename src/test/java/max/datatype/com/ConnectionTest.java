/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author Önder Gürcan
 *
 */
public class ConnectionTest {

	@Test
	public void getLastCommunicationTime() {
		Address address = new UUIDAddress();
		Connection<Address, Double> connection = new Connection<>(address, 0, 3.0, Connection.IN);
		assertEquals(3.0, connection.getLastCommunicationTime(), 0.0);

		double time = 10.0;
		connection.setLastCommunicationTime(time);
		assertEquals(time, connection.getLastCommunicationTime(),0.0);

		assertEquals(address, connection.getAddress());

		assertEquals(Connection.IN, connection.getConnectionDirection());
	}

}

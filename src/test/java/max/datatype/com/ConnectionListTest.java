/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author Önder Gürcan
 *
 */
public class ConnectionListTest {

	private static final int MAX_OUTBOUND_CONNECTIONS = 3;
	private static final int MAX_INBOUND_CONNECTIONS = 10;

	@Test
	public void addConnection() {
		ConnectionList<Address, Integer> connectionList = new ConnectionList<>(MAX_OUTBOUND_CONNECTIONS, MAX_INBOUND_CONNECTIONS);
		Address address = new UUIDAddress();
		connectionList.addConnection(new Connection<>(address, 0, 0, Connection.OUT));
		assertEquals(1, connectionList.getNumberOfOutConnections());

		Address address2 = new UUIDAddress();
		connectionList.addConnection(new Connection<>(address2, 0, 0, Connection.IN));
		assertEquals(1, connectionList.getNumberOfInConnections());
	}

	@Test
	public void removeConnection() {
		ConnectionList<Address, Integer> connectionList = new ConnectionList<>(MAX_OUTBOUND_CONNECTIONS, MAX_INBOUND_CONNECTIONS);
		Address address = new UUIDAddress();
		Connection<Address, Integer> connection = new Connection<>(address, 0, 0, Connection.IN);
		connectionList.addConnection(connection);
		assertEquals(1, connectionList.getNumberOfInConnections());
		connectionList.removeConnection(connection);
		assertEquals(0, connectionList.getNumberOfInConnections());
	}

	@Test
	public void contains() {
		ConnectionList<Address, Integer> connectionList = new ConnectionList<>(MAX_OUTBOUND_CONNECTIONS, MAX_INBOUND_CONNECTIONS);
		Address address = new UUIDAddress();
		Connection<Address, Integer> connection = new Connection<>(address, 0, 0, Connection.IN);
		connectionList.addConnection(connection);
		assertTrue(connectionList.contains(address));
	}

}

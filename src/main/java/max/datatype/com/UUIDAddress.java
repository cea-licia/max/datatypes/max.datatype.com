/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import java.util.UUID;

/**
 * A universally unique identifier (UUID) is a 128-bit number used to identify
 * information in computer systems. {@link UUIDAddress} is used to generate addresses
 * using UUID.
 * 
 * @author Önder Gürcan
 */
public class UUIDAddress extends Address {

	/** The UUID of this message. */
	private final UUID id;

	/**
	 * Creates a new {@link UUIDAddress} instance with a generated UUID.
	 */
	public UUIDAddress() {
		this.id = UUID.randomUUID();
	}

	/**
	 * Creates a new {@link UUIDAddress} instance with the provided UUID.
	 *
	 * @param str the UUID
	 * @throws IllegalArgumentException the provided UUID can't be decoded
	 */
	public UUIDAddress(String str) {
		this.id = UUID.fromString(str);
	}

	@Override
	public String toString() {
		return id.toString();
	}
	
}
/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import static max.datatype.com.Contact.DEFAULT;

import java.util.ArrayList;
import java.util.List;

/**
 * A list of {@link Contact}.
 *
 * @author Önder Gürcan
 */
public class ContactList {

	/** The list of contacts. */
	private final List<Contact> contactList;

	/**
	 * Creates an empty {@link ContactList}.
	 */
	public ContactList() {
		this.contactList = new ArrayList<Contact>();
	}

	/**
	 * Adds the provided {@link Contact} to the contacts list.
	 *
	 * @param newContact contact to add
	 */
	public void addContact(Contact newContact) {
		this.contactList.add(newContact);
	}

	/**
	 * Removes the provided contact from the contacts list.
	 *
	 * @param contact contact to remove
	 */
	public void removeContact(Contact contact) {
		this.contactList.remove(contact);
	}

	/**
	 * Gets the contact at the specified index.
	 *
	 * @param number index of the contact to retrieve
	 * @return the contact
	 * @throws IndexOutOfBoundsException the index is out of the contacts list boundaries.
	 */
	public Contact getContact(int number) {
		return this.contactList.get(number);
	}

	/**
	 * For each contact, gets the address associated to the provided key and collects all addresses in
	 * a list.
	 *
	 * @param type the key
	 * @return a list of addresses. Some addresses might be {@code null}.
	 * @deprecated method is deprecated and shouldn't be used
	 */
	@Deprecated(since = "1.2.0")
	public List<Address> getAllContactAddresses(String type) {
		List<Address> result = new ArrayList<Address>();
		//
		for (Contact contact : this.contactList) {
			Address address = contact.getAddress(type);
			result.add(address);
		}
		return result;
	}

	/**
	 * Collects all the contacts' names in a list.
	 *
	 * @return a collection of names.
	 */
	public List<String> getAllContacts() {
		List<String> result = new ArrayList<String>();
		//
		for (Contact contact : this.contactList) {
			String name = contact.getName();
			result.add(name);
		}
		return result;
	}

	/**
	 * Gets the first contact which has the provided address associated to the default key.
	 *
	 * If no such contact exists then a new default contact is created and the provided address added
	 * to it using the default address key.
	 *
	 * @param address provided address
	 * @return a contact in the list or a default and created contact (not in the list)
	 */
	public Contact getContact(Address address) {
		for (Contact contact : this.contactList) {
			if (contact.getAddress(DEFAULT).equals(address)) {
				return contact;
			}
		}
		Contact unknownContact = new Contact("Unknown");
		unknownContact.addAddress(DEFAULT, address);
		return unknownContact;
	}

	/**
	 * Gets the contacts list size.
	 *
	 * @return the size.
	 */
	public int size() {
		return this.contactList.size();
	}

}

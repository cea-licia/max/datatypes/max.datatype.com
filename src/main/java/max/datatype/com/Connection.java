/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

/**
 * Creates a connection class with parameters Address (Default address of the
 * neighbor) the ban-score, which will be set if there is any misbehaviour by the
 * neighbor, lastCommunicationTime to tell if the Neighbor is Active, and the
 * Connection Direction which is if the connection is incoming or outgoing.
 *
 * @author Önder Gürcan
 */
public class Connection<A, T> {

	/**
	 * Address DEFAULT of the neighbor
	 */
	private final A neighborAddress;

	/**
	 * Ban-score of the neighbor, if there is misbehaviour from this neighbor, it is
	 * incremented, then the neighbor is removed if reached the threshold
	 */
	// private int banScore;

	/**
	 * lastCommunicationTime with the neighbor, if a message is received from this
	 * neighbor, it is reset, but if no msg is received in a certain time, the
	 * neighbor is removed
	 */
	private T lastCommunicationTime;

	/**
	 * if the connection is incoming or outgoing
	 */
	private final String connectionDirection;

	/**
	 * Static strings
	 */
	public static String OUT = "OUT";
	public static String IN = "IN";

	public Connection(A address, int score, T lastCommunicationTime, String connectionDirection) {

		this.neighborAddress = address;

		// this.banScore = score;

		this.lastCommunicationTime = lastCommunicationTime;

		this.connectionDirection = connectionDirection;

	}

	public T getLastCommunicationTime() {
		return this.lastCommunicationTime;
	}

	public void setLastCommunicationTime(T time) {
		this.lastCommunicationTime = time;
	}

	public A getAddress() {
		return this.neighborAddress;
	}

	public String getConnectionDirection() {
		return this.connectionDirection;
	}

}

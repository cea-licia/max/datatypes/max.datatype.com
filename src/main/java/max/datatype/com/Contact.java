/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import java.util.Hashtable;

/**
 * A class to store a contact.
 *
 * <p>Each contact has a name and zero or more addresses. {@link Contact} objects are mainly used in
 * {@link ContactList}.
 *
 * @see ContactList
 * @author Önder Gürcan
 */
public class Contact {

	/** Name of the contact. */
	private String name;

	/** Default name for an unnamed contact. */
	public static final String NONAME = "NONAME";

	/** Addresses storage. */
	private final Hashtable<String, Address> address;

	/** Default key for addresses. */
	public static final String DEFAULT = "DEFAULT";

	/** Error message when the user tries to set a {@code null} address. */
	public static final String E_NULL_ADDRESS_TYPE = "The type of the address cannot be null.";

	/** Error message when the user tries to set a {@code null} name. */
	public static final String E_NULL_CONTACT_NAME = "The name of the contact cannot be null.";

	/**
	 * Default constructor.
	 *
	 * Creates a {@link Contact} with the default name and no addresses.
	 */
	public Contact() {
		this(NONAME);
	}

	/**
	 * Creates a {@link Contact} given a name. No addresses.
	 *
	 * @param name contact's name
	 */
	public Contact(String name) {
		setName(name);
		this.address = new Hashtable<>();
	}

	/**
	 * Adds the given address to the contact.
	 *
	 * The address can be retrieved using the provided key. If the key was already associated to
	 * another address, then the former address is replaced by the new one.
	 *
	 * @param key the key to retrieve the address
	 * @param address the address to store
	 * @throws IllegalArgumentException the provided key is {@code null}
	 */
	public void addAddress(String key, Address address) {
		if (key != null) {
			this.address.put(key, address);
		} else {
			throw new IllegalArgumentException(E_NULL_ADDRESS_TYPE);
		}
	}

	/**
	 * Sets the contact's name.
	 *
	 * @param name the new name
	 * @throws IllegalArgumentException {@code name} is {@code null}
	 */
	public void setName(String name) {
		if (name != null) {
			this.name = name;
		} else {
			throw new IllegalArgumentException(E_NULL_CONTACT_NAME);
		}
	}

	/**
	 * Gets the contact name.
	 *
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the address associated to the provided key.
	 *
	 * @param key the key to retrieve the address
	 * @return either the address or {@code null} if no address is associated to the key
	 */
	public Address getAddress(String key) {
		return this.address.get(key);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (name == null) {
			return other.name == null;
		} else return name.equals(other.name);
	}

}

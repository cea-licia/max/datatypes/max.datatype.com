/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import java.util.ArrayList;
import java.util.List;

/**
 * A message is used for interactions taking place in an environment. A message
 * is ignorant to the agents and artifacts and deals only with addresses.
 * 
 * @author Önder Gürcan
 * @author Aimen Djari
 * @param <A> address's type
 * @param <P> payload's type
 */
public class Message<A,P> implements Comparable<Message<A,P>> {

	/**
	 * The type of this message.
	 */
	protected String type;

	/**
	 * A key that may be used to count messages according to some criteria.
	 */
	protected String countKey;

	/**
	 * The address of the sender of this message.
	 */
	protected final A sender;

	/**
	 * The address(es) of the receiver(s) of this message.
	 */
	private final List<A> receivers;

	/**
	 * The payload of this message. It is assumed that based on the protocol, the
	 * type of payload is known.
	 */
	protected final P payload;

	/**
	 * Creates a message from {@code sender}, to each receiver in {@code receivers} and with the
	 * provided {@code payload}.
	 *
	 * @param sender who sends the message
	 * @param receivers recipients of the message
	 * @param payload content of the message
	 */
	public Message(A sender, List<A> receivers, P payload) {
		this.sender = sender;
		this.receivers = receivers;
		this.payload = payload;
	}

	/**
	 * Creates a message from {@code sender}, to {@code receiver} and with the provided
	 * {@code payload}.
	 *
	 * @param sender who sends the message
	 * @param receiver recipient of the message
	 * @param payload content of the message
	 */
	public Message(A sender, A receiver, P payload) {
		this.sender = sender;
		this.receivers = new ArrayList<>();
		this.receivers.add(receiver);
		this.payload = payload;
	}

	/**
	 * Gets this message's sender.
	 *
	 * @return the sender
	 */
	public A getSender() {
		return this.sender;
	}

	/**
	 * Gets the list of recipients of the message.
	 *
	 * @return a list of recipients
	 */
	public List<A> getReceivers() {
		return this.receivers;
	}

	/**
	 * Gets this message payload.
	 *
	 * @return the payload
	 */
	public P getPayload() {
		return payload;
	}

	/**
	 * Sets this message's type.
	 *
	 * @param type message type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets this message's type.
	 *
	 * @return the message type
	 */
	public String getType() {
		return type;
	}


	/**
	 * Sets this message's count key.
	 *
	 * @param countKey message count key
	 */
	public void setCountKey(String countKey) {
		this.countKey = countKey;
	}

	/**
	 * Gets this message's count key.
	 *
	 * @return the message count key
	 */
	public String getCountKey() {
		return countKey;
	}

	@Override
	public String toString() {
		return "Message [type=" + type + ", sender=" + sender + ", receivers=" + receivers + ", payload=" + payload
				+ "]";
	}

	@Override
	public int compareTo(Message<A,P> arg0) {
		return 0;
	}

}

/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.datatype.com;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

/**
 * Creates a connection class with parameters {@link Address} (default address of the
 * neighbor), lastCommunicationTime to tell if the neighbor is active, and the
 * connection direction which is if the connection is incoming or outgoing.
 *
 * @author Mohamed Aimen Djari
 * @author Önder Gürcan
 */
public class ConnectionList<A, T> {

	/** A mapping neighbor to connection. */
	private final Hashtable<A, Connection<A, T>> connections;

	/** The maximum size of the outgoing neighbors' list. */
	private int maxOutboundConnections = 3;

	/** The maximum size of the incoming neighbors' list. */
	private int maxInboundConnections = 10;

	/** The owner of the {@link ConnectionList}. */
	private A owner;

	/**
	 * Creates a new {@link ConnectionList}.
	 *
	 * @param maxOutboundConnections The maximum size of the outgoing neighbor's list.
	 * @param maxInboundConnections The maximum size of the incoming neighbor's list.
	 */
	public ConnectionList(final int maxOutboundConnections, final int maxInboundConnections) {
		this.connections = new Hashtable<A, Connection<A, T>>();
		
		this.maxOutboundConnections = maxOutboundConnections;
		this.maxInboundConnections = maxInboundConnections;

	}

	/**
	 * Adds the provided connection to the list.
	 *
	 * In order to add the connection, the method checks:
	 * <ol>
	 *   <li>if the list already contains a {@link Connection} associated to the address</li>
	 *   <li>if not, the number of inbound / outbound connections must also be respected</li>
	 * </ol>
	 *
	 * If one of the two criteria is not fulfilled, the connection is silently ignored.
	 *
	 * @param connection the connection to add
	 */
	public void addConnection(Connection<A, T> connection) {
		if (!(this.contains(connection.getAddress()))) {
			if (connection.getConnectionDirection() == Connection.IN) {
				if (this.getNumberOfInConnections() < maxInboundConnections) {
					this.connections.put(connection.getAddress(), connection);
				}
			} else if (connection.getConnectionDirection() == Connection.OUT) {
				if (this.getNumberOfOutConnections() < maxOutboundConnections) {
					this.connections.put(connection.getAddress(), connection);
				}
			}
		}
	}

	/**
	 * Removes the provided connection from the connection list.
	 *
	 * @param inactiveNeighbor connection to remove
	 */
	public void removeConnection(Connection<A, T> inactiveNeighbor) {
		connections.remove(inactiveNeighbor.getAddress());
	}

	/**
	 * Gets the number of incoming connections in the list of the connections.
	 * 
	 * @return a positive number.
	 */
	public int getNumberOfInConnections() {
		int inConnectionsCount = 0;
		Iterator<Connection<A, T>> iterator = this.connections.values().iterator();
		while (iterator.hasNext()) {
			Connection<A, T> connection = iterator.next();
			if (connection.getConnectionDirection() == Connection.IN) {
				inConnectionsCount++;
			}
		}
		return inConnectionsCount;
	}

	/**
	 * Gets the number of outgoing connections in the list of the connections.
	 * 
	 * @return a positive integer
	 */
	public int getNumberOfOutConnections() {
		int outConnectionsCount = 0;
		Iterator<Connection<A, T>> iterator = this.connections.values().iterator();
		while (iterator.hasNext()) {
			Connection<A, T> connection = iterator.next();
			if (connection.getConnectionDirection() == Connection.OUT) {
				outConnectionsCount++;
			}
		}
		return outConnectionsCount;
	}

	/**
	 * Returns the list of the neighbors' addresses to be able to communicate with
	 * them.
	 * 
	 * @return a possibly empty list of neighboring addresses.
	 */
	public List<A> getNeighborsAddresses() {
		List<A> neighborsList = new ArrayList<A>();
		Iterator<Connection<A, T>> iterator = connections.values().iterator();
		while (iterator.hasNext()) {
			Connection<A, T> connection = iterator.next();
			neighborsList.add(connection.getAddress());
		}
		return neighborsList;
	}

	/**
	 * Gets the internal connections mapping (address to connection).
	 *
	 * @return the mapping addresses to connection
	 */
	public Hashtable<A, Connection<A, T>> getConnectionsList() {
		return this.connections;
	}

	/**
	 * Sets the last communication time to now when a message is received from the
	 * neighbor's Address or if it has just been added to the list.
	 * 
	 * @param neighborAddress the neighbor's address
	 * @param time the last communication time
	 */
	public void setLastCommunicationTime(A neighborAddress, T time) {

		if (contains(neighborAddress)) {
			Connection<A, T> connection = get(neighborAddress);
			connection.setLastCommunicationTime(time);
		}
	}

	/**
	 * Checks if the neighbor's address given in parameter is in the list of
	 * connections.
	 * 
	 * @param neighborAddress the provided address
	 * @return {@code true} if the address is in the list, else {@code false}
	 */
	public boolean contains(A neighborAddress) {
		Connection<A, T> connection = connections.get(neighborAddress);
		return connection != null;
	}

	/**
	 * Clears the connections list.
	 */
	public void clear() {
		this.connections.clear();
	}

	/**
	 * Gets the size of this connections list.
	 *
	 * @return a positive integer
	 */
	public int size() {
		return connections.size();
	}

	/**
	 * Given an address, gets the corresponding connection.
	 *
	 * @param address provided address
	 * @return either the connection if it exists, {@code null} otherwise.
	 */
	public Connection<A, T> get(A address) {
		return connections.get(address);
	}

	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public A getOwner() {
		return this.owner;
	}

	/**
	 * Sets this connections list's owner.
	 *
	 * @param address the owner
	 */
	public void setOwner(A address) {
		this.owner = address;
	}

	/**
	 * Gets the maximum outbound connections number allowed for this connections list.
	 *
	 * @return positive integer
	 */
	public int getMaxOutboundConnections() {
		return maxOutboundConnections;
	}

	/**
	 * Gets the maximum inbound connections number allowed for this connections list.
	 *
	 * @return positive integer
	 */
	public int getMaxInboundConnections() {
		return maxInboundConnections;
	}
}
